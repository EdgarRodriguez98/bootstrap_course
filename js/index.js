 $(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('[data-toggle="popover"]').popover();

        $('.carousel').carousel({
          interval:5000
        });

        $("#exampleModal").on('show.bs.modal',function(e){
          console.log("El modal se esta mostrando");
          $("#contactBtn").addClass("btn-primary");
          $("#contactBtn").prop("disabled", true);
        });

        $("#exampleModal").on('shown.bs.modal',function(e){
          console.log("El modal se mostro por completo");
        });

        $("#exampleModal").on('hide.bs.modal',function(e){
          console.log("El modal se esta cerrando");
          $("#contactBtn").removeClass("btn-primary");
          $("#contactBtn").prop("disabled", false);
        });

        $("#exampleModal").on('hidden.bs.modal',function(e){
          console.log("El modal se cerró por completo");
        });
      });  